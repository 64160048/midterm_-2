package com.thitiphong;

public class App {
    public static void main(String[] args) {
        Record monkung = new Record("monkung", 20000.0); 
        monkung.print();
        Record sompong = new Record("somepong", 10000.0);
        sompong.print();
        sompong.income(500.0);
        sompong.print();

        monkung.expenses(2000.0);
        monkung.print();

    }

}
