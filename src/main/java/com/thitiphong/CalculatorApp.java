package com.thitiphong;

import java.util.Scanner;

public class CalculatorApp {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Calculator cal = new Calculator();
        System.out.printf("Please input num1 : ");
        int num1 = sc.nextInt();
        System.out.printf("%nPlease input num2 : ");
        int num2 = sc.nextInt();

        System.out.println(cal.sum(num1, num2));
        System.out.println(cal.minus(num1, num2));
        System.out.println(cal.multiply(num1, num2));
        System.out.println(cal.divide(num1, num2));
        System.out.println(cal.pow(num1, num2));

    }

}