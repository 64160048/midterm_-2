package com.thitiphong;

public class Record {
    private String list;
    private double balance;

    public Record(String list, double balance) {
        this.list = list;
        this.balance = balance;
    }

    public boolean income(double money) {

        balance = balance + money;
        return true;
    }

    public boolean expenses(double money) {
        if (money < 1)
            return false;
        if (money > balance)
            return false;
        balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(list + " " + balance);
    }

    public String getList() {
        return list;
    }

    public double getBalance() {
        return balance;
    }
}
