package com.thitiphong;

public class Calculator {

    public  int sum(int num1, int num2) {
        return num1 + num2;
    }

    public  int minus(int num1, int num2) {
        return num1 - num2;
    }

    public  int multiply(int num1, int num2) {
        return num1 * num2;
    }

    public  double divide(float num1, float num2) {
        return num1 / num2;
    }

    public  int pow(int num, int numPow) {
        int total = 1;
        for (int i = 0; i < numPow; i++) {
            total *= num;
        }
        return total;

    }

}
