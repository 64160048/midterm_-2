package com.thitiphong;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RecordTest {
    @Test
    public void ExpensesWithNegativeNumber1() {
        Record record1 = new Record("monkung" , 100);
        record1.expenses(-200);
        assertEquals(100, record1.getBalance(),0.00001);
    }
    @Test
    public void ExpensesSuccess() {
        Record record1 = new Record("monkung" , 100);
        record1.expenses(50);
        assertEquals(50, record1.getBalance(),0.00001);
    }
    
    @Test
    public void ExpensesOverbalance() {
        Record record1 = new Record("monkung" , 100);
        record1.expenses(150);
        assertEquals(100, record1.getBalance(),0.00001);
    }
    @Test
    public void ExpensesWithNegativeNumber2() {
        Record record1 = new Record("monkung" , 100);
        record1.expenses(-100);
        assertEquals(100, record1.getBalance(),0.00001);
    }
    
    @Test
    public void ExpensesWithNegativeNumber() {
        Record record1 = new Record("monkung" , 100);
        record1.expenses(-100);
        assertEquals(100, record1.getBalance(),0.00001);
    }
    @Test
    public void IncomeWithNegativeNumber() {
        Record record1 = new Record("monkung" , 100);
        record1.income(-100);
        assertEquals(0, record1.getBalance(),0.00001);
    }
}
